# Xilinx Vivado 2019.1 installation

**IMPORTANT NOTE:** all the project paths must not have spaces in the folder names

We are going to use **Vivado 2019.1** and **XSDK** as a development environment. It is important you install **this** version.
![vivado_logo](imgs/vivado_logo.png)  


We will use Vivado to design the processing systems.
A processing system is composed by interconnected modules that, after the synthesis and implementation processes, will be downloaded into the FPGA chip of the developement board.
![block_design](imgs/block_design.jpg)  

XSDK (Xilinx Software Development Kit) is the software, included in the Xilinx Vivado suite, intended to design and compile the software that will run on the processing systems' microcontrollers.
XSDK is built on Eclipse.

![xsdk](imgs/xsdk.jpg)  

Be sure you install the version 2019.1, you find it in Xilinx download Archive:
https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive.html 

Before the download you’ll be asked to register an account, do it. Username and password will be required during the installation.
Download the self installer for your OS and run it.

![download](imgs/download.png)  

If prompted to install the newest version, refuse it and continue with Vivado 2019.1  
![continue](imgs/continue.png)  

Choose `Download and Install Now`:  
![install_now](imgs/install_now.png)  

Choose Vivado HL WebPACK  
![webpack](imgs/webpack.png)  

The required disk space is about 35 GB.


In the customization page be sure to select every feature listed in the picture below:  
![requirements](imgs/requirements.png)  

Then choose the installation path. Do not use spaces in the folder names!

## Installing support for Zybo
We are going to use Zybo as a development board.

At this link you may download the board definitions: 
[Zybo Board Definitions files](https://drive.google.com/file/d/17jbn0UXZSFRTO-55UzScuCIvpu2psAZd/view?usp=sharing)  
extract the archive and copy the zybo folder in `<vivado_directory>\Vivado\2017.1\data\boards\board_files`  
if you are on Linux you could have to manage with permissions.

Then restart vivado.


## Possible error with C++ libraries on Windows
After the installation ends successfully an error with C++ libraries could happen:


At this link are suggested a few solutions: 
https://www.xilinx.com/support/answers/69076.html

The following solution usually solves the problem.

> Bypassing the vcredist check
> On some machines, the check for a valid install of vcredist 2015 is failing even though it has been properly installed. We are looking into why the check fails so that this can be fixed in a future release.
> However, in this case, the check can be bypassed. To bypass the vcredist check when the 2015 version of the Microsoft redistributables has already been installed manually or by another program, set the following environment variable:
> `XIL_PA_NO_REDIST_CHECK = 1`


If you still get the error, try restarting the PC.


***AGAIN, IMPORTANT NOTE:*** all the project paths must not have spaces in the folder names


